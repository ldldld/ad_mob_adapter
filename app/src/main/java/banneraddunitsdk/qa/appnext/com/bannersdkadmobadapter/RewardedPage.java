package banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.ads.fullscreen.FullScreenVideo;
import com.appnext.ads.fullscreen.RewardedConfig;
import com.appnext.ads.fullscreen.RewardedServerSidePostback;
import com.appnext.ads.fullscreen.RewardedVideo;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEvent;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobRewardedVideoAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class RewardedPage extends AppCompatActivity implements View.OnClickListener{

    private Button loadDefaultRewarded;
    private Button loadCustomRewarded;
    private Button rewardedSetters;

    private RewardedVideoAd customEventRewarded;
    private RewardedVideoAd defaultEventRewarded;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewarded_page);

        loadDefaultRewarded = findViewById(R.id.loadRewardedDefault);
        loadDefaultRewarded.setOnClickListener(this);

        loadCustomRewarded = findViewById(R.id.loadRewardedCustom);
        loadCustomRewarded.setOnClickListener(this);

        rewardedSetters = findViewById(R.id.rewardedSetters);
        rewardedSetters.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.loadRewardedDefault):
                defaultEventRewarded = MobileAds.getRewardedVideoAdInstance(this);
                Bundle defaultEventExtrasRewarded = new Bundle();
                defaultEventRewarded.loadAd("ca-app-pub-4981994338959649/6878021014",
                        new AdRequest.Builder().
                                addNetworkExtrasBundle(AppnextAdMobRewardedVideoAdapter.class,
                                        defaultEventExtrasRewarded).build());
                callBacks(defaultEventRewarded);

                break;

            case(R.id.loadRewardedCustom):

                customEventRewarded = MobileAds.getRewardedVideoAdInstance(this);
                RewardedConfig rewardedConfig = new RewardedConfig();
                rewardedConfig.setPostback("Your_Postback_Params");
                rewardedConfig.setCategories("Categories");
                rewardedConfig.setMute(false);
                //rewardedConfig.setVideoLength(FullScreenVideo.VIDEO_LENGTH_DEFAULT);
                rewardedConfig.setOrientation(FullScreenVideo.ORIENTATION_LANDSCAPE);
                rewardedConfig.setMinVideoLength(4);
                rewardedConfig.setMinVideoLength(8);
                rewardedConfig.setMode("multi");


                RewardedServerSidePostback rewardedServerSidePostback = new RewardedServerSidePostback();
                rewardedServerSidePostback.setRewardsTransactionId("aa");
                rewardedServerSidePostback.setRewardsUserId("bb");
                rewardedServerSidePostback.setRewardsCustomParameter("cc");
                rewardedServerSidePostback.setRewardsRewardTypeCurrency("dd");
                rewardedServerSidePostback.setRewardsAmountRewarded("eee");

                Bundle customEventExtrasRewarded = new Bundle();
                customEventExtrasRewarded.putSerializable(AppnextAdMobCustomEvent
                        .AppnextConfigurationExtraKey, rewardedConfig);
                customEventExtrasRewarded.putSerializable(AppnextAdMobCustomEvent
                        .AppnextRewardPostbackExtraKey, rewardedServerSidePostback);
                customEventRewarded.loadAd("ca-app-pub-4981994338959649/6878021014",
                        new AdRequest.Builder()
                                .addNetworkExtrasBundle(AppnextAdMobRewardedVideoAdapter.class,
                                        customEventExtrasRewarded).build());
                callBacks(customEventRewarded);

                break;

            case(R.id.rewardedSetters):

                break;
        }
    }

    public void callBacks(final RewardedVideoAd rewardedEvent){
        rewardedEvent.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                Toast.makeText(RewardedPage.this, "Rewarded Ad Loaded", Toast.LENGTH_SHORT).show();
                rewardedEvent.show();

            }

            @Override
            public void onRewardedVideoAdOpened() {
                Toast.makeText(RewardedPage.this, "Rewarded Ad Opened", Toast.LENGTH_SHORT).show();
                // mCustomEventRewardedButton.setEnabled(false);
            }

            @Override
            public void onRewardedVideoStarted() {

            }

            @Override
            public void onRewardedVideoAdClosed() {
                Toast.makeText(RewardedPage.this, "Rewarded Ad Closed", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onRewarded(RewardItem rewardItem) {

            }

            @Override
            public void onRewardedVideoAdLeftApplication() {
//                Toast.makeText(BannersPage.this,"Rewarded Ad Closed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {
                Toast.makeText(RewardedPage.this,
                        "Error loading custom event rewarded, code ",//errorCod
                        Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RewardedPage.this, LandingPage.class));
        finish();
    }


}
