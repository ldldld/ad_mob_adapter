package banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter.BannersSizes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.banners.BannerAdRequest;
import com.appnext.sdk.adapters.admob.banners.AppnextAdMobBannerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter.BannersPage;
import banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter.LandingPage;
import banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter.R;

import static com.appnext.banners.BannerAdRequest.TYPE_VIDEO;
import static com.appnext.banners.BannerAdRequest.VIDEO_LENGTH_LONG;

public class SmallBanner extends AppCompatActivity implements View.OnClickListener {

    private AdView mAdView;
    private BannerAdRequest configSmallCus;
    private BannerAdRequest configSmallDef;
    private Button loadDefaultSmallBanner;
    private Button loadCustomSmallBanner;
    private Button smallBannerSetters;


    private Button reloadAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_small_banner);

        loadDefaultSmallBanner = findViewById(R.id.loadDefaultSmallBanner);
        loadDefaultSmallBanner.setOnClickListener(this);

        loadCustomSmallBanner = findViewById(R.id.loadCustomSmallBanner);
        loadCustomSmallBanner.setOnClickListener(this);

        smallBannerSetters = findViewById(R.id.smallBannerSetters);
        smallBannerSetters.setOnClickListener(this);
        mAdView = findViewById(R.id.adViewSmall);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.loadDefaultSmallBanner):
                if (mAdView != null) {
                    mAdView.destroy();
                }
                configSmallDef = new BannerAdRequest();
                Bundle extras = new Bundle();
                extras.putSerializable(AppnextAdMobBannerAdapter.AppNextBannerAdRequestKey, configSmallDef);
                AdRequest adRequestDefault = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobBannerAdapter.class, extras).build();
                mAdView.loadAd(adRequestDefault);
                callBacks();
                break;

            case(R.id.loadCustomSmallBanner):
                if (mAdView != null) {
                    mAdView.destroy();
                }
                configSmallCus = new BannerAdRequest();
                Bundle extrasSmall = new Bundle();
                configSmallCus.setCategories("Games, Adventure")
                        .setPostback("AdMob Installed");
                extrasSmall.putSerializable(AppnextAdMobBannerAdapter.AppNextBannerAdRequestKey, configSmallCus);
                AdRequest adRequestCustom = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobBannerAdapter.class, extrasSmall).build();

                mAdView.loadAd(adRequestCustom);
                callBacks();
                break;
        }
    }

    public void callBacks() {
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                Toast.makeText(SmallBanner.this, "Banner Loaded", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.

                Toast.makeText(SmallBanner.this, "Banner Load Error", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
                Toast.makeText(SmallBanner.this, "Banner Opened", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
                Toast.makeText(SmallBanner.this, "Banner Left App", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the user is about to return
                // to the app after tapping on an ad.
                Toast.makeText(SmallBanner.this, "Banner Closed", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mAdView != null) {
            mAdView.destroy();
        }
        startActivity(new Intent(SmallBanner.this, BannersPage.class));
        finish();
    }
}
