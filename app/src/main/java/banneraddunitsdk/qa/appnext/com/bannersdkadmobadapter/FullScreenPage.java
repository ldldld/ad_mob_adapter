package banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.ads.fullscreen.FullScreenVideo;
import com.appnext.ads.fullscreen.FullscreenConfig;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEvent;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEventFullScreenVideo;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class FullScreenPage extends AppCompatActivity implements View.OnClickListener{

    private Button loadDefaultFs;
    private Button loadCustomFs;
    private Button fsSetters;
    private InterstitialAd customFullScreen;
    private InterstitialAd defaultFullScreen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_page);

        loadDefaultFs = findViewById(R.id.loadFsDefault);
        loadDefaultFs.setOnClickListener(this);

        loadCustomFs = findViewById(R.id.loadFsCustom);
        loadCustomFs.setOnClickListener(this);

        fsSetters = findViewById(R.id.fsSetters);
        fsSetters.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.loadFsDefault):
                defaultFullScreen = new InterstitialAd(this);
                defaultFullScreen.setAdUnitId("ca-app-pub-4981994338959649/3057401011");

                FullscreenConfig fullscreenDef = new FullscreenConfig();
                Bundle defaultEventExtrasFS = new Bundle();
                defaultEventExtrasFS.putSerializable(AppnextAdMobCustomEvent.AppnextConfigurationExtraKey, fullscreenDef);
                AdRequest adRequestFSDefault = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobCustomEventFullScreenVideo.class, defaultEventExtrasFS).build();
                defaultFullScreen.loadAd(adRequestFSDefault);
                callBacks(defaultFullScreen);
                break;

            case (R.id.loadFsCustom):

                customFullScreen = new InterstitialAd(this);
                customFullScreen.setAdUnitId("ca-app-pub-4981994338959649/3057401011");
                FullscreenConfig fullscreenConfig = new FullscreenConfig();
                fullscreenConfig.setPostback("Admob Fullscreen");
                fullscreenConfig.setCategories("Shopping");
                fullscreenConfig.setMute(false);
                fullscreenConfig.setBackButtonCanClose(true);
                fullscreenConfig.setVideoLength(FullScreenVideo.VIDEO_LENGTH_LONG);
                fullscreenConfig.setOrientation(FullScreenVideo.ORIENTATION_PORTRAIT);
                fullscreenConfig.setMinVideoLength(4);
                fullscreenConfig.setMaxVideoLength(4);
                fullscreenConfig.setShowClose(true, 6000);
                Bundle customEventExtrasFS = new Bundle();
                customEventExtrasFS.putSerializable(AppnextAdMobCustomEvent.AppnextConfigurationExtraKey, fullscreenConfig);
                AdRequest adRequestFSCustom = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobCustomEventFullScreenVideo.class, customEventExtrasFS).build();
                customFullScreen.loadAd(adRequestFSCustom);
                callBacks(customFullScreen);
                break;

            case (R.id.fsSetters):

                break;
        }
    }

    public void callBacks(final InterstitialAd fullScreenEvent){
        fullScreenEvent.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(FullScreenPage.this,
                        "Error loading customized custom event fullscreen, code " + errorCode,
                        Toast.LENGTH_SHORT).show();
                // mCustomisedCustomEventFullScreenButton.setEnabled(true);
            }

            @Override
            public void onAdLoaded() {
                Toast.makeText(FullScreenPage.this, "Fullscreen Ad Loaded", Toast.LENGTH_SHORT).show();
                fullScreenEvent.show();

                //mCustomisedCustomEventFullScreenButton.setEnabled(true);
            }

            @Override
            public void onAdOpened() {
                Toast.makeText(FullScreenPage.this, "Fullscreen Ad Opened", Toast.LENGTH_SHORT).show();
                //mCustomisedCustomEventFullScreenButton.setEnabled(false);
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(FullScreenPage.this, "Fullscreen Ad Closed", Toast.LENGTH_SHORT).show();
                // configAndLoadCustomisedFullScreen();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FullScreenPage.this, LandingPage.class));
        finish();
    }
}
