package banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LandingPage extends AppCompatActivity implements View.OnClickListener{

    private Button goToBanners;
    private Button goToInterstitial;
    private Button goToFullScreen;
    private Button goToRewarded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);

        goToBanners = findViewById(R.id.banners);
        goToBanners.setOnClickListener(this);

        goToInterstitial = findViewById(R.id.interstitial);
        goToInterstitial.setOnClickListener(this);

        goToFullScreen = findViewById(R.id.fullScreen);
        goToFullScreen.setOnClickListener(this);

        goToRewarded = findViewById(R.id.rewarded);
        goToRewarded.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case (R.id.banners):
                startActivity(new Intent(LandingPage.this, BannersPage.class));
                finish();
                break;

            case (R.id.interstitial):
                startActivity(new Intent(LandingPage.this, InterstitialPage.class));
                finish();
                break;

            case (R.id.fullScreen):
                startActivity(new Intent(LandingPage.this, FullScreenPage.class));
                finish();
                break;

            case (R.id.rewarded):
                startActivity(new Intent(LandingPage.this, RewardedPage.class));
                finish();
                break;
        }
    }
}
