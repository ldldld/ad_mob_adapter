package banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter.BannersSizes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.banners.BannerAdRequest;
import com.appnext.sdk.adapters.admob.banners.AppnextAdMobBannerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter.BannersPage;
import banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter.R;

import static com.appnext.banners.BannerAdRequest.TYPE_VIDEO;
import static com.appnext.banners.BannerAdRequest.VIDEO_LENGTH_LONG;

public class LargeBanner extends AppCompatActivity implements View.OnClickListener{

    private AdView mAdView;
    private BannerAdRequest configLargeCus;
    private BannerAdRequest configLargeDef;
    private Button loadDefaultLargeBanner;
    private Button loadCustomLargeBanner;
    private Button largeBannerSetters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_large_banner);
        loadDefaultLargeBanner = findViewById(R.id.loadDefaultLargeBanner);
        loadDefaultLargeBanner.setOnClickListener(this);
        loadCustomLargeBanner = findViewById(R.id.loadCustomLargeBanner);
        loadCustomLargeBanner.setOnClickListener(this);
        largeBannerSetters = findViewById(R.id.settersLarge);
        largeBannerSetters.setOnClickListener(this);
        mAdView = findViewById(R.id.adViewLarge);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.loadDefaultLargeBanner):
                if (mAdView != null) {
                    mAdView.destroy();
                }
                configLargeDef = new BannerAdRequest();
                Bundle extras = new Bundle();
                extras.putSerializable(AppnextAdMobBannerAdapter.AppNextBannerAdRequestKey, configLargeDef);
                AdRequest adRequestDefault = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobBannerAdapter.class, extras).build();
                mAdView.loadAd(adRequestDefault);
                callBacks();
                break;

            case(R.id.loadCustomLargeBanner):
                if (mAdView != null) {
                    mAdView.destroy();
                }
                configLargeCus = new BannerAdRequest();
                Bundle extrasSmall = new Bundle();
                configLargeCus.setCategories("Games, Adventure")
                        .setPostback("AdMob Installed");
                extrasSmall.putSerializable(AppnextAdMobBannerAdapter.AppNextBannerAdRequestKey, configLargeCus);
                AdRequest adRequestCustom = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobBannerAdapter.class, extrasSmall).build();

                mAdView.loadAd(adRequestCustom);
                callBacks();
                break;
        }
    }

    public void callBacks() {
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Toast.makeText(LargeBanner.this, "Banner Loaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(LargeBanner.this, "Banner Load Error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                Toast.makeText(LargeBanner.this, "Banner Opened", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(LargeBanner.this, "Banner Left App", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(LargeBanner.this, "Banner Closed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mAdView != null) {
            mAdView.destroy();
        }
        startActivity(new Intent(LargeBanner.this, BannersPage.class));
        finish();
    }
}
