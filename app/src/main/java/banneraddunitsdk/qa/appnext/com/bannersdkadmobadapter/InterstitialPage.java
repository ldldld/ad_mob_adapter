package banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
//

import com.appnext.ads.interstitial.Interstitial;
import com.appnext.ads.interstitial.InterstitialConfig;
import com.appnext.base.Appnext;
import com.appnext.core.Ad;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEvent;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEventInterstitial;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class InterstitialPage extends AppCompatActivity implements View.OnClickListener{

    private InterstitialAd customInterstitial;
    private InterstitialAd defaultInterstitial;
    private InterstitialAd mCustomisedCustomEventFullScreen;
    private Button loadCustomInterstitial;
    private Button showCustomInterstitial;
    private Button loadDefaultInterstitial;
    private Button showDefaultInterstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial_page);

        loadCustomInterstitial = findViewById(R.id.loadCustomInterstitial);
        loadCustomInterstitial.setOnClickListener(this);

        showCustomInterstitial = findViewById(R.id.isCustomLoaded);
        showCustomInterstitial.setOnClickListener(this);
        showCustomInterstitial.setEnabled(false);

        loadDefaultInterstitial = findViewById(R.id.loadDefaultInterstitial);
        loadDefaultInterstitial.setOnClickListener(this);

        showDefaultInterstitial = findViewById(R.id.isDefaultLoaded);
        showDefaultInterstitial.setOnClickListener(this);
        showDefaultInterstitial.setEnabled(false);
        Appnext.init(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.loadCustomInterstitial):
                InterstitialConfig config = new InterstitialConfig();
                config.setButtonColor("#6AB344");
                config.setPostback("Your_Postback_Params");
                config.setCategories("Categories");
                config.setSkipText("Skip");
                config.setMute(false);
                config.setAutoPlay(true);
                config.setCreativeType(Interstitial.TYPE_MANAGED);
                config.setOrientation(Ad.ORIENTATION_DEFAULT);
                Bundle customEventExtras = new Bundle();
                customEventExtras.putSerializable(AppnextAdMobCustomEvent.AppnextConfigurationExtraKey, config);
                AdRequest adRequestInterstitial = new AdRequest.Builder().addCustomEventExtrasBundle(AppnextAdMobCustomEventInterstitial.class, customEventExtras).build();
                customInterstitial = new InterstitialAd(this);
                customInterstitial.setAdUnitId("ca-app-pub-4981994338959649/8964333816");
                customInterstitial.loadAd(adRequestInterstitial);
                callBack(customInterstitial, "custom");
                break;

            case (R.id.isCustomLoaded):
                customInterstitial.show();
                break;

            case (R.id.loadDefaultInterstitial):
                defaultInterstitial = new InterstitialAd(this);
                defaultInterstitial.setAdUnitId("ca-app-pub-4981994338959649/8964333816");
                defaultInterstitial.loadAd(new AdRequest.Builder().build());
                callBack(defaultInterstitial, "default");
                break;

            case (R.id.isDefaultLoaded):
                defaultInterstitial.show();
                break;
        }
    }

    public void callBack(InterstitialAd interstitialAd, final String interstitialType){
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(InterstitialPage.this,
                        "Error loading customized custom event interstitial, code " + errorCode,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded() {
                if(interstitialType.equals("custom")){
                    showCustomInterstitial.setEnabled(true);
                }
                if(interstitialType.equals("default")){
                    showDefaultInterstitial.setEnabled(true);
                }

                Toast.makeText(InterstitialPage.this, "Interstitial Ad Loaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                Toast.makeText(InterstitialPage.this, "Interstitial Ad Opened", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(InterstitialPage.this, "Interstitial Ad Closed", Toast.LENGTH_SHORT).show();
                if(interstitialType.equals("custom")){
                    showCustomInterstitial.setEnabled(false);
                }
                if(interstitialType.equals("default")){
                    showDefaultInterstitial.setEnabled(false);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(InterstitialPage.this, LandingPage.class));
        finish();
    }
}
