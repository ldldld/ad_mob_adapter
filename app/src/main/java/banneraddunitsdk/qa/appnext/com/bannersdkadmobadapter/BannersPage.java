package banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appnext.ads.fullscreen.FullScreenVideo;
import com.appnext.ads.fullscreen.FullscreenConfig;
import com.appnext.ads.fullscreen.RewardedConfig;
import com.appnext.ads.fullscreen.RewardedServerSidePostback;
import com.appnext.banners.BannerAdRequest;
import com.appnext.sdk.adapters.admob.banners.AppnextAdMobBannerAdapter;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEvent;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobCustomEventFullScreenVideo;
import com.appnext.sdk.adapters.admob.ads.AppnextAdMobRewardedVideoAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter.BannersSizes.LargeBanner;
import banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter.BannersSizes.MediumBanner;
import banneraddunitsdk.qa.appnext.com.bannersdkadmobadapter.BannersSizes.SmallBanner;

import static com.appnext.banners.BannerAdRequest.TYPE_VIDEO;
import static com.appnext.banners.BannerAdRequest.VIDEO_LENGTH_LONG;


public class BannersPage extends AppCompatActivity implements View.OnClickListener{

    private Button bannerBtn;
    private Button mediumBtn;
    private Button largeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banners_page);
        bannerBtn = findViewById(R.id.bannerBtn);
        bannerBtn.setOnClickListener(this);
        mediumBtn = findViewById(R.id.mediumBtn);
        mediumBtn.setOnClickListener(this);
        largeBtn = findViewById(R.id.largeBtn);
        largeBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.bannerBtn):
                startActivity(new Intent(BannersPage.this, SmallBanner.class));
                finish();
                break;

            case (R.id.mediumBtn):
                startActivity(new Intent(BannersPage.this, MediumBanner.class));
                finish();
                break;

            case (R.id.largeBtn):
                startActivity(new Intent(BannersPage.this, LargeBanner.class));
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(BannersPage.this, LandingPage.class));
        finish();
    }
}
